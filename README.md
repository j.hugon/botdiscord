# Bot JDR Discord
## Pré - requis
Pour utiliser ce bot il vous faudra node js et npm.   
Documentation disponible [ici](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)
## Instalation
- Utilisez `npm install` a la racine pour installer les différentes librairie necessaires.   
- Il vous faudra ensuite un token discord [un super lien qui explique comment faire](https://discordpy.readthedocs.io/en/latest/discord.html).   
Une fois ce token obtenu mettez le dans le fichier `config.json`.
- N'oubliez pas d'inviter le bot a votre server [c'est le même lien mais pas la même partie](https://discordpy.readthedocs.io/en/latest/discord.html#inviting-your-bot)
## Lancement du bot
Utilisez la commande `node client.js`.   
Et hop le tour est joué !
## Utilisation du bot
Tapez `!help` dans discord pour avoir le manuel d'utilisation.
## Bon jeu !