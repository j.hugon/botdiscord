# Aide utilisation du bot
⚠️Bot en developpement ⚠️
Contactez "johann.hugon@pm.me" en cas de questions, problèmes, suggestions, insultes, mots d'amour.
## Lancer de dés :
 "! 'nombre de dés' 'lettre d ou D' 'valeur maximale du dé'"
 Exemple : !1d100 pour lancer 1 dé 100
## Statistiques
### Affichage
 !stats
 Les statistiques ne concernent que les dés 100, il faut donc en avoir lancé au moins un pour pouvoir utiliser la commande !
### Modification
#### Coups Critiques
 !SetCC La_Nouvelle_Valeur
 Modifie la valeur de vos coups critiques
 Exemple : !SetCC 7 pour mettre les CC a 7
#### Echecs Critiques
 !SetEC La_Nouvelle_Valeur
 Modifie la valeur de vos échecs critiques
 Exemple : !SetEC 97 pour mettre les EC a 97
### Suppression
 !reset
 Permet de supprimer toutes les données sur le joueur.
