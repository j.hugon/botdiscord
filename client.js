const Discord = require("discord.js");
const fs = require("fs");
const { token } = require("./config.json");

const prefix = "!";
const bot = new Discord.Client();

const ressources_file = "Ressources/";
const data_player_file = "Joueurs/";
const template = ressources_file + "Template.json";
const help = ressources_file + "help.md";
const { exec } = require("child_process");

function addMarkdown(mess) {
  return "```markdown" + "\n" + mess + "\n" + "```";
}
function getHelp(mess) {
  return addMarkdown(
    mess + "\nPeut être que " + `${prefix}` + "help peut vous aider ! "
  );
}

bot.on("message", (message) => {
  if (message.author.bot) return;

  if (message.content.startsWith(`${prefix}`)) {
    mess = message.content.replace(`${prefix}`, "").toLowerCase();
    file = data_player_file + message.author.username + ".json";
    //Test d'existence du dossier Joueurs/ et creation via template si non
    if (!fs.existsSync(data_player_file)) {
      fs.mkdirSync(data_player_file);
    }
    //Test d'existence du fichier Joueur et creation via template si non
    if (!fs.existsSync(file)) {
      fs.copyFileSync(template, file);
      console.log(file + " is initialised !");
    }
    let rawdata = fs.readFileSync(file);
    let player = JSON.parse(rawdata);

    //Lancé de dé
    if (mess.includes("d")) {
      tab = mess.split("d");
      tab[0] = Math.abs(Number(tab[0]));
      tab[1] = Math.abs(Number(tab[1]));
      if (isNaN(tab[0]) || isNaN(tab[1])) {
        message.channel.send(getHelp("Mauvaise syntaxe !"));
        return;
      }
      mess = "# " + tab[0] + "d" + tab[1] + " = \n";
      for (let step = 0; step < tab[0]; step++) {
        result = Math.floor(Math.random() * tab[1]) + 1;
        mess += step + 1 + " : ";
        mess += result;
        if (tab[1] == 100) {
          if (result >= player.ec) {
            mess += " FUCK";
          } else if (result <= player.cc) {
            mess += " Coup critique! Bien joué " + message.author.username;
          }
          player.score[player.score.length] = result;
        }
        mess += "\n";
      }
       message.channel.send(addMarkdown(mess)).catch(err =>{
          if(err.code == 50035){
            message.reply("le resultats est plus grand que les 2000 caractères tolérés par Discord :/ ");
          }
          console.error(err);
        });
    } else if (mess == "stats") {
      if (!player.score.length) {
        message.channel.send(getHelp("Lancez des dés pour avoirs des stats !"));
      } else {
        total = 0;
        cc = 0;
        ec = 0;
        for (let i = 0; i < player.score.length; i++) {
          val = player.score[i];
          if (val >= player.ec) ec++;
          else if (val <= player.cc) cc++;
          total += player.score[i];
        }
        mess = "# STAT :\n";
        mess +=
          (total / player.score.length).toFixed(2) +
          " pour " +
          player.score.length +
          " lancé de dé 100 !\n" +
          "Echec critique (dés au dessus de " +
          player.ec +
          "): " +
          ec +
          "\nCoup critique (dés en dessous de " +
          player.cc +
          "): " +
          cc;
        message.channel.send(addMarkdown(mess));
      }
    } else if (mess == "help") {
      mess = fs.readFileSync("Ressources/help.md", "utf8");
      message.channel.send(addMarkdown(mess));
    } else if (mess.includes("setcc")) {
      tab = mess.split(" ");
      tab[1] = Number(tab[1]);
      if (isNaN(tab[1])) {
        message.channel.send(getHelp("Mauvaise syntaxe !"));
      } else {
        player.cc = tab[1];
        message.channel.send(
          addMarkdown("Valeur de coup critique mise à " + player.cc)
        );
      }
    } else if (mess.includes("setec")) {
      tab = mess.split(" ");
      tab[1] = Number(tab[1]);
      if (isNaN(tab[1])) {
        message.channel.send(getHelp("Mauvaise syntaxe !"));
      } else {
        player.ec = tab[1];
        message.channel.send(
          addMarkdown("Valeur d'echec critique mise à " + player.ec)
        );
      }
    } else if (mess.includes("reset")) {
      player = JSON.parse(fs.readFileSync(template));
      console.log(file + " is reinitialised !");
      message.channel.send(addMarkdown("Stats remises à zéro !"));
    } else {
      message.channel.send(getHelp("G PA KOMPRI"));
    }
    let data = JSON.stringify(player, null, 2);
    fs.writeFileSync(file, data);
  }

  if (message.content.toLowerCase() === "ping") {
    message.reply("pong !");
  }
});
bot.once("ready", function () {
  console.log("Je suis connecté !");
});

bot.once("reconnecting", () => {
  console.log("Reconnecting!");
});

bot.once("disconnect", () => {
  console.log("Disconnect!");
});

if (!fs.existsSync(ressources_file)) {
  console.log("Folder Ressources is missing !");
  process.exit();
} else if (!fs.existsSync(template)) {
  console.log("Template ressource is missing !");
  process.exit();
} else if (!fs.existsSync(help)) {
  console.log("Help ressource is missing !");
  process.exit();
} else {
  bot.login(token);
}
